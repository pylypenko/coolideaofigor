from database import *

class Products(Model):
    id = AutoField()
    name = CharField(max_length = 50)
    description = TextField()
    category = IntegerField()
    price = IntegerField()
    store = IntegerField()
    active = BooleanField()
    count = IntegerField()
    author = IntegerField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    
    class Meta:
        database = db
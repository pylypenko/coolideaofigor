from database import *
from product import Products

def get_products(query):
    query = query.lower().strip()
    return Products.select().where(Products.name.contains(query) | Products.description.contains(query))
from database import *

class Stores(Model):
    id = AutoField()
    name = CharField(max_length = 50)
    created_at = DateTimeField()
    updated_at = DateTimeField()

    class Meta:
        database = db
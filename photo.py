from database import *

class Photos(Model):
    id = AutoField()
    filename = CharField(max_length = 50)
    product_id = IntegerField()
    created_at = DateTimeField()
    updated_at = DateTimeField()

    class Meta:
        database = db
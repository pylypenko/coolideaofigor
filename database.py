from config import CONFIG
from playhouse.pool import PooledMySQLDatabase
from peewee import *

db = PooledMySQLDatabase(CONFIG['Database'],
                    user=CONFIG['User'],
                    password=CONFIG['Password'],
                    host=CONFIG['Host']
                    )
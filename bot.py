from main import *
from photo import Photos
from string import Template
from telebot.types import InputMediaPhoto
import requests
import urllib
import telebot

TOKEN = '750501861:AAHmqfg8m4X9wvuKgtTxZ1agx2HqQsf-4mw'

bot = telebot.TeleBot(TOKEN)
template = Template(open('template.txt', encoding="utf-8").read())

@bot.message_handler(commands=['start'])
def send_instruction(message):
	bot.send_message(message.chat.id, 'Вводиш запит - отримуєш відповідь')

@bot.message_handler(content_types=['text'])
def reply(message):
    products = get_products(message.text)
    if len(products) == 0:
        bot.send_message(message.chat.id, 'Вибачте, таким не торгуємо')
    else:
        bot.send_message(message.chat.id, 'Можу запропонувати такі товари:')
        for product in products:
            bot.send_message(message.chat.id,
                                template.substitute(
                                    name = product.name,
                                    description = product.description,
                                    price = product.price,
                                    count = product.count,
                                ))

            photos = Photos.select().where(Photos.product_id == product.id)
            bot.send_media_group(message.from_user.id, [InputMediaPhoto(urllib.request.urlopen(photo.filename)) for photo in photos])
            

bot.polling()
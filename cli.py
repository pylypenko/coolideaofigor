# This is cli file
'''
    python cli.py migrate - for migrations
'''

import sys

if len(sys.argv) > 1:
    if sys.argv[1] == 'migrate':
        exec(open('./migrate.py').read())
        print('Migrations done successfully')
    else:
        print('Invalid command')
else:
    print('Help')
    print('======================================')
    print('python cli.py migrate - for migrations')